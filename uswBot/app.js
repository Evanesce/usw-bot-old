/* Program: USW Gaming and Esport Bot
 * Description: main bot for the USW gaming and esports discord
 *      
 * Developer: Evanesce (Jack Clements)
 */

//constant variables and requires for use throughout
const Discord = require('discord.js');
const bot = new Discord.Client();
const fs = require('fs');
const mysql = require('mysql');
const nodemailer = require('nodemailer');
const csv = require('fast-csv');
const config = require('./assets/config.json');


/* Logging function
 * used to keep track of command usage, member activity
 * and any uncaught errors
 */
function log(text)
{
    let logChannel = bot.channels.find("name", "bot-log");
    console.log(text);
    logChannel.send(text);
    fs.appendFile("assets/log.logfile", text + "\n", (err) =>
    {
        if (err) throw err;
    });
};


//database setup connection
var db_config = {
    host: config.sqlHost,
    user: config.sqlUser,
    password: config.sqlPass,
    database: config.sqlDatabaseName
};

process.on("uncaughtException", (err) =>
{
    log(`Uncaught Exception: ${err}`)
});

bot.on("error", (err) =>
{
    log(`client error: ${err}`);
    bot = new Discord.Client();
});

bot.on("uncaughtException", (err) =>
{
    log(`client exception: ${err}`);
    bot = new Discord.Client();
});




//config for mail service
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.mailEmail,
        pass: config.mailPassword
    }
});
    
//sql connection variable
var connection;


/* handleDisconnect
 * allowed rejoining of sql database asyncronously
 * in case of a disconnect or network disruption
 */
function handleDisconnect() {
    connection = mysql.createConnection(db_config); // Recreate the connection, since
    // the old one cannot be reused.

    connection.connect(function (err) {              // The server is either down
        if (err) {                                     // or restarting (takes a while sometimes).
            log('error when connecting to db reconnecting in 2 seconds');
            console.log(err);
            setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
    // If you're also serving http, display a 503 error.
    connection.on('error', function (err) {
        log('db error');
        console.log(err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } else {                                      // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
    //console.log("reconnect successfull")
}
handleDisconnect();


/*try to force mysql to keep connection alive
 *
 */
setInterval(function () { connection.query("SELECT 1") }, 5000);



/* update currency
 * updates a users currency in the sql database
 */
function updateCurrency(userID, amount) {
   // log(`debug: ${userID}, ${amount}`);
    connection.query("SELECT wealth FROM Members WHERE discordID = '" + userID + "'", function (err, rows, fields)
    {
        if (err)
            return log(`err @ updateCurrency(): ${err}`);

        amount = parseInt(amount);

        let balance = parseInt(rows[0].wealth);
        let newBalance = amount + balance;
        connection.query("UPDATE Members SET wealth = ? WHERE discordID = ?", [newBalance, userID]);
    });
}


/* reactionMeme
 * Base code for the reaction meme response commands
 */
function reactionMeme(memePath, message)
{
    connection.query("SELECT * FROM Members WHERE discordID = ?", [message.author.id], function (err, rows, fields)
    {
        if (err)
            return log(`error in reaction meme: ${err}`);
        //check for enough money
        if (rows[0].wealth >= config.reactionMemePrice)
        {
            message.author.send(`${config.reactionMemePrice} coins have been taken out of your wallet!`);
            updateCurrency(message.author.id, 0 - config.reactionMemePrice);
        }
        else
        {
            message.reply(`I'm sorry you don't have enough coins to do that!`);
            return;
        }

        //print meme
        message.channel.sendFile(`assets/images/${memePath}`);
    });

}


/* emailUser
 *  emails a user with a given student id on the southwales email network
 *  takes two args:
 *  studentID - the id of the student
 *  message - to see if a message reply should be given or not
 */
function emailUser(studentID, message)
{
    //setup options for the invite
    var options = {
        maxAge: 86400,
        maxUses: 1,
        unique: true
    }
    let inviteChannel = bot.channels.find("name", "welcome");
    var invite = inviteChannel.createInvite(options).then(function (invite)
    {


        //setup options for the email
        var mailOptions = {
            from: 'USW Gaming',
            to: `${studentID}@students.southwales.ac.uk`,
            subject: "Discord Invitation!",
            html: `<h3>Hello and welcome to the university of South Wales Gaming and E-Sports society!</h3><br />\
<p>The society will run weekly and monthly events, these events will be anything from a drinking social in\
the randy dragon to gaming lans in swansea. The monthly events will Have a &#163;3 entry fee to generate some\
income for the society to pay for some event costs, you can bypass this by paying &#163;15 upfront (can be whenever\
you have the money) to avoid the &#163;3 event cost.</p>\
<p>Join the discord here: https://discord.gg/${invite.code} - you will find a link to Facebook in the discord welcome chat.<br />
Good luck & have fun\n\n\n\n\</p>
<p>This discord invite code lasts for 1 day, if your invite has expired and need a new one please reply to this email and you will be invited again asap.</p>`
        };

        //send the email
        transporter.sendMail(mailOptions, function (error, info)
        {
            if (error)
            {
                log(` inviteUser error: ${error}`);
            }
            else
            {
                //skip this log for bulk invitations
                if (message)
                    log(`Email sent to student with invite ${invite.code}: ${studentID} - ${info.response}`);
            }
        });

        //reply if a message object was given
        if (message)
            return message.reply(`Email invite sent to ${studentID}`);
    });
}


/*check walletAcess
 * prints error message and returns value of access
 * 
 */
function checkWalletAccess(hasAccess, message)
{
    if (hasAccess == 0)
    {
        message.reply("You don't have access to your wallet, contact an admin for further infomation.");
        return false;
    }
    return true;

}


/* getHelpMessage
 * Just returns the help message
 * channel
 * 
 */
function getHelpMessage()
{
    let levelOneMessage = `\`\`\`asciidoc\n\
= Game Role Commands = \`\`\`\
\`\`\`css\n\
 - !assignGame   =>   Adds a game role to yourself\n\
     ->  { Example: '!assignGame CSGO' }\n\
 - !listGames    =>   Lists all the currently avalible game roles\n\
 - !createGame   =>   Creates a new game role if it doesn't exist \n\
     ->  { Example: '!createGame Dota2' }   [costs ${config.createGamePrice} coins]\n\
\`\`\`\n\
\`\`\`asciidoc\n\
= Currency System Commands =\`\`\`\
\`\`\`css\n\
 - !wallet       =>   Displays your current balance in coins\n\
 - !pay          =>   Pays selected user designated amount\n\
     ->  { Example: '!pay 50 @Evanesce' }\n\
 - !cointoss     =>   Bet on a coin toss: pays double the bet\n\
     ->  { Example: '!coinToss heads 3' }\`\`\`\n\
\`\`\`asciidoc\n
= Super Emotes = \`\`\`\
\`\`\`css\n\
[All Cost ${config.reactionMemePrice} coins]\n\
 - !???          =>   Confused Guy\n\
 - !spongeCave   =>   Who's there?\n\
 - !everyone     =>   No one Loves me\n\
 - !gandalf      =>   Bangin' tunes\n\
 - !persianCat   =>   When your coins disappear\n\
 - !rage         =>   No Description Needed\`\`\`\n\n`


    return levelOneMessage;
}



////////////////////////////////////////////////////////
//           End Of Function Declerations             //
////////////////////////////////////////////////////////





/* catch exception and throw log
 * use this to prevent closing of application on errors
 * log them for later use
 */
process.on('uncaughtException', function (exception) {
    log(exception);
});


/* call init method from the Bot object
 * opens the websocket for the bot
 */
bot.on('ready', () => {
    bot.user.setActivity("!help for commands!");
  //  log("Client ready!");
});


/* guildMemberRemove
 * logs a user leaving the discord
 */
bot.on("guildMemberRemove", (member) => {
    log(`${member.user} has left ${member.guild.name}`);
});


/* guildMemberAdd
 * logs a member joining the discord
 */
bot.on("guildMemberAdd", member => {
    let guild = member.guild;
    log(`${member.user} joined ${guild.name}`);



    //create member info
    var info = {
        "discordID": member.user.id.toString(),
        "wealth": config.startingWealth,
        "lastMessageTime": "0",
        "hasWalletAccess": "1"
    }

    connection.query("SELECT * FROM Members WHERE discordID = ?", [member.user.id], function (err, rows, fields)
    {
        //checking if user is in database
        try
        {
            log(`${rows[0].discordID} already exists in the database`);
        }
        catch(err)
        {
            //add member to database
            connection.query("INSERT INTO Members SET?", info, error =>
            {
                if (error)
                {
                    log(`Error adding member: ${error}`);
                }
                else
                {
                    log(`Successfully added ${member.user} to the database!`);
                }
            });
        }
    });

    //welcome message
    let message = `Welcome ${member.user} to the USW Gaming and E-Sports society discord!\n\
You will be unable to post for the next 10 minutes, please use this time to read through the\
 ${guild.channels.find("name", config.rulesChannel)} and ${guild.channels.find("name", config.coinChannel)} channels!\n\
It may also be beneficial to take a look in the  ${guild.channels.find("name", config.commandsChannel)} channel.`
    guild.channels.find("name", config.welcomeChannel).send(message);

});




//Message sent
bot.on('message', message => {

    // just reuturn the function if bot is messaged in an invalid way
    //could change this later for access to bot commands via dm
    if (message.author.bot) return;
    if (message.channel.isPrivate) return;
    if (message.channel.type == "dm") return;


    //loading role names from config.json
    let developer = config.developer;
    let admin = config.admin;
    //setting role names to variables
    let developerRole = message.guild.roles.find("name", developer);
    let adminRole = message.guild.roles.find("name", admin);


    //timing variables for hourly pay
    let lastMessage = "";
    let messageNew = new Date().getTime() / 1000;




    //Update last message for player as well as currency every hour
    connection.query("SELECT lastMessageTime FROM Members WHERE discordID = '" + message.author.id + "'", function (err, rows, fields) {
        if (err)
            return log(`err @ userMessage: ${err}`);
        try {
            lastMessage = rows[0].lastMessageTime;
            //adds x coin(s) to user's wallet every hour
            if (messageNew - parseInt(lastMessage) > 3600) {
                message.member.send(`congratulations ${message.author}, you just got ${config.hourlyRate} coin for being active!`);
                updateCurrency(message.author.id, config.hourlyRate);
                log(`Gave ${message.author} ${config.hourlyRate} coin to their wallet`);
                connection.query("UPDATE Members SET lastMessageTime = ? WHERE discordID = ?", [messageNew, message.author.id]);
            }
            
        }
        catch (err) {
            //add the user to the database since they don't exist for some reason
            var info = {
                "discordID": message.author.id,
                "wealth": config.startingWealth,
                "lastMessageTime": "0",
                "hasWalletAccess": "1"
            }
            //add member to database
            connection.query("INSERT INTO Members SET?", info, error => {
                if (error) {
                    log(`Error adding member: ${error}`);
                }
            })
            log(`Successfully added ${message.author} to the database!`);
        }
    });






    /* Start of text commands
     * All command conditions here
     */
    if (!message.content.startsWith(config.prefix)) { return }
    else
    {
        //command splicing
        let command = message.content.split(" ")[0];
        command = command.slice(config.prefix.length);
        let args = message.content.split(" ").slice(1);
        command = command.toLowerCase();

            ////////////////////////////////////////////////////////
            //             Start of betting commands              //
            ////////////////////////////////////////////////////////

        //check for correct channel
        if (message.channel.name == config.bettingChannel)
        {
            if (command == "cointoss")
            {
                let amount = args[1];
                let bettingOption = args[0][0].toLowerCase();

                //check for h/t input
                if (bettingOption != "h" && bettingOption != "t")
                    return message.reply(`Usage: ${config.prefix}coinToss [heads/tails] [amount]`);

                //check for NaN
                if (isNaN(amount) || amount < 0 || amount % 1 != 0)
                    return message.reply("Please enter a valid amount to bet!");

                connection.query("SELECT * FROM Members WHERE discordID = ?", [message.author.id], function (err, rows, fields)
                {
                    if (err)
                        return log(`err @ cointoss: ${err}`);

                    //check wallet for currency
                    if (rows[0].wealth < amount)
                        return message.reply("You don't have enough coins for that!");

                    let rng = Math.floor((Math.random() * 100) + 1);
                    let output = "NULL";

                    //calculate coin

                    if (rng <= 50)
                        output = 'Heads';
                    else
                        output = 'Tails';

                    message.channel.send(`The coin landed on ${output}!`);

                    //update currency of player
                    if (bettingOption == output[0].toLowerCase())
                    {
                        message.reply(`You have gained ${amount} coins!`);
                    }
                    else
                    {
                        message.reply(`You have lost ${amount} coins!`);
                        amount = -amount;
                    }

                    updateCurrency(message.author.id, amount);

                    log(`${message.author} used cointoss and obtained ${amount} coin(s)`);

                });

            }
        }


            ////////////////////////////////////////////////////////
            //             Start of normal commands               //
            ////////////////////////////////////////////////////////

        //check for correct channel
        if (message.channel.name == config.commandsChannel || message.member.roles.has(adminRole.id))
        {
            /* addgame
             * Adds a game role to yourself
             */
            if (command == "assigngame" || command == "ag")
            {
                let roleArg = args[0];

                connection.query("SELECT * FROM RoleList WHERE roleName = '" + roleArg + "'", function (err, rows, fields)
                {
                    if (err)
                        return log(`err @ assignGame: ${err}`);

                    try
                    {
                        //test to see if role exists
                        let tempRole = rows[0].roleName

                        if (roleArg == tempRole)
                        {
                            message.member.addRole(message.guild.roles.find("name", roleArg));
                            message.reply(`You now have the role: ${roleArg}`);
                            log(`${message.author} assigned ${roleArg} to themself`);
                        }
                        else
                        {
                            message.reply("Please make sure you typed the role correctly.");
                        }
                    }
                    catch (err)
                    {
                        message.reply(`${roleArg} doesn't exist in the database, use !createGame ${roleArg} to add it!`);
                        log(`${message.author} unsuccessfully used assignGame`)
                    }

                });

            }//end assigngame


            /* listGames
             * list all entries in the RoleList Tables
             */
            if (command == "listgames" || command == "lg")
            {
                try
                {
                    connection.query("SELECT * FROM RoleList", function (err, rows, fields)
                    {
                        if (err)
                            return log(`err @ listgames: ${err}`);

                        outputString = "```css\n Number Of Games: " + rows.length;

                        for (var i = 0; i < rows.length; i++)
                        {
                            outputString += `\n[${rows[i].roleName}]`;
                        }

                        outputString += "```";

                        message.channel.send(outputString);

                        log(`${message.author} used listgames`);
                    });
                }
                catch (err)
                {
                    log(`Jack go fix listgames: ${err}`)
                }
            }//end listgames


            /* Help
             * dms the user a list of all relevent commands to their
             * permission level
             */
            if (command == "help" || command == "commands")
            {
                let levelTwoMessage = `\n\`\`\`asciidoc\n\
= Admin Commands =\`\`\`\
\`\`\`css\n\
 - !kick         =>   Kicks a member\n\
 - !ban          =>   Bans a member\n\
 - !give         =>   Gives a member x currency (+ve or -ve)\n\
     ->  { Example: '!give 50 @Evanesce' }\n\
 - !adminAddRole =>   Adds any role to a members\n\
     ->  { Example: '!adminAddRole someRole @Evanesce' }\n\
 - !deleteGame   =>   Deletes a game Role
     ->  { Example: '!DeleteGame CSGO' }\n\
 - !editGame     =>   Changes the name of an existing game role\n\
     ->  { Example: '!editGame CSGO CounterStrike' }\n\
 - !clear        =>   Clears a number of messages in a channel
     ->  { Example: '!clear 8' }\n\
 - !getID        =>   Get's the ID of the tagged user\n\
 - !nukeWallets  =>   Resets all Members currency to zero [must confirm]\n\
     ->  { Usage: '!nukeWallets Yes I Am Sure' }\n\
 - !toggleWallet =>   Toggles the wallet of the mentioned user\n\
 - !say          =>   Make the bot send a message in the desired channel\n\
     ->  { Example: '!say Announcements Urgent Update coming soon!' }\n\
 - !updateBotCommands => Updates the bot commands channel\n\
 - !inviteUser   =>   Invites a user to discord via email (student number)\n\
         { Example: '!inviteUser 170435132' }\n\
 - !bulkInviteUsers   => Sends email invites to all users in invite list \`\`\``

                message.member.send(getHelpMessage());

                if (message.member.roles.has(adminRole.id))
                    message.member.send(levelTwoMessage);

                message.reply("You have been sent a list of commands!");
                log(`${message.author} used help command`);
            }


            ////////////////////////////////////////////////////////
            //          start of currency commands here           //
            ////////////////////////////////////////////////////////

            //check for disabled wallet
            connection.query("SELECT * FROM Members WHERE discordID = ?", [message.author.id], function (err, walletRows, fields)
            {
                if (err)
                    return log(`err @ WalletChecking: ${err}`);

                if (command == "cointoss")
                {
                    let amount = args[1];
                    let bettingOption = args[0][0].toLowerCase();

                    //check for h/t input
                    if (bettingOption != "h" && bettingOption != "t")
                        return message.reply(`Usage: ${config.prefix}coinToss [heads/tails] [amount]`);

                    //check for NaN
                    if (isNaN(amount) || amount < 0 || amount % 1 != 0)
                        return message.reply("Please enter a valid amount to bet!");

                    connection.query("SELECT * FROM Members WHERE discordID = ?", [message.author.id], function (err, rows, fields)
                    {
                        if (err)
                            return log(`err @ cointoss: ${err}`);

                        //check wallet for currency
                        if (rows[0].wealth < amount)
                            return message.reply("You don't have enough coins for that!");

                        let rng = Math.floor((Math.random() * 100) + 1);
                        let output = "NULL";

                        //calculate coin

                        if (rng <= 50)
                            output = 'Heads';
                        else
                            output = 'Tails';

                        message.channel.send(`The coin landed on ${output}!`);

                        //update currency of player
                        if (bettingOption == output[0].toLowerCase())
                        {
                            message.reply(`You have gained ${amount} coins!`);
                        }
                        else
                        {
                            message.reply(`You have lost ${amount} coins!`);
                            amount = -amount;
                        }

                        updateCurrency(message.author.id, amount);

                        log(`${message.author} used cointoss and obtained ${amount} coin(s)`);

                    });

                }

                /* Wallet
                 * allows a user to see their balance or
                 * allows an admin to see anyone's balance
                 */
                if (command == "wallet")
                { //self checking

                    //check for access
                    if (!checkWalletAccess(walletRows[0].hasWalletAccess, message))
                        return;

                    if (!message.member.roles.has(adminRole.id) || (message.member.roles.has(adminRole.id) && message.mentions.users.size == 0))
                    {
                        connection.query("SELECT wealth FROM Members WHERE discordID = '" + message.author.id + "'", function (err, rows, fields)
                        {
                            if (err)
                                return log(`err @ wallet: ${err}`);
                            let balance = parseInt(rows[0].wealth);
                            message.reply(`You currently have ${balance} Coins!`);
                            log(`${message.author} used wallet command in  ${message.channel} with non-mod permissions`);
                        });
                    }
                    else
                    { //mod checking
                        connection.query("SELECT wealth FROM Members WHERE discordID = '" + message.mentions.users.first().id + "'", function (err, rows, fields)
                        {
                            if (err)
                                return log(`err @ wallet2: ${err}`);
                            let balance = parseInt(rows[0].wealth);
                            message.reply(`${message.mentions.users.first()} currently has ${balance} Coins!`);
                            log(`${message.author} used wallet command in ${message.channel} with mod permissions`);
                        });
                    }
                }//end wallet


                /* Pay
                 * Pay a user coins by giving them from your wallet
                 */
                if (command == "pay")
                {//Pay a user from your wallet

                    //check for access
                    if (!checkWalletAccess(walletRows[0].hasWalletAccess, message))
                        return;

                    let user1 = message.author.id;
                    let wallet;
                    let amount = parseInt(args[0]);
                    let targetUserWallet = 0;
                    //check for NaN
                    if (isNaN(amount) || amount < 0 || amount % 1 != 0)
                    {
                        message.reply("Please enter a valid amount!");
                        log(`${message.author} incorrectly used pay command in ${message.channel}`);
                        return;
                    }
                    //Check for no users mentioned
                    if (message.mentions.users.size == 0)
                    {
                        return message.reply(`Please mention a user to give ${amount} Coins to!!`);
                        log(`${message.author} incorrectly used pay command in ${message.channel}`);
                        return 0;
                    }
                    else
                    {
                        let user2 = message.mentions.users.first().id;


                        //get wallet info
                        connection.query("SELECT wealth FROM Members WHERE discordID = '" + user1 + "'", function (err, rows, fields)
                        {
                            if (err)
                                return log(`err @ pay: ${err}`);
                            wallet = parseInt(rows[0].wealth);
                            connection.query("SELECT wealth FROM Members WHERE discordID = '" + user2 + "'", function (err, rows, fields)
                            {
                                targetUserWallet = parseInt(rows[0].wealth);
                                if (amount > wallet)
                                {
                                    message.reply(`You don't have enough Coins to do that!`);
                                    log(`${message.author} incorrectly used pay command in ${message.channel}`);
                                    return;
                                }
                                let targetUserNew = targetUserWallet + amount;
                                let userNew = wallet - amount;
                                //update wallets
                                connection.query("UPDATE Members SET wealth = ? WHERE discordID = ?", [targetUserNew, user2]);
                                connection.query("UPDATE Members SET wealth = ? WHERE discordID = ?", [userNew, user1]);
                                log(`${message.author} paid ${message.mentions.users.first()} ${amount} Coins in ${message.channel}`);
                                message.reply(` you have paid ${message.mentions.users.first()} ${amount} Coins!`);
                                return;
                            });
                        });
                    }
                }//end of pay


                /* Create a role for a new game
                 * adds that game to both sql database and discord
                 * roles
                 */
                if (command == "creategame" || command == "cg")
                {

                    //check for access
                    if (!checkWalletAccess(walletRows[0].hasWalletAccess, message))
                        return;

                    //check for enough money
                    if (walletRows[0].wealth >= config.createGamePrice)
                    {
                        message.author.send(`${config.createGamePrice} coins have been taken out of your wallet!`);
                        updateCurrency(message.author.id, 0 - config.createGamePrice);
                    }
                    else
                    {
                        message.reply(`I'm sorry you don't have enough coins to do that!`);
                        return;
                    }

                    //start command

                    let roleArg = args[0];

                    //set data for new roles
                    var data = {
                        "roleName": roleArg
                    }

                    //insert into mysql database
                    connection.query("INSERT INTO RoleList SET?", data, error =>
                    {
                        if (error)
                        {
                            log(`Error at create game. Dump:  ${error}`);
                        }

                        //create the role on discord
                        message.guild.createRole({
                            name: roleArg,
                            mentionable: true,
                        });
                    });

                    message.reply(`[${roleArg}] Successfully added to games!`);
                    log(`${message.author.username} created game ${roleArg}`);
                }//end creategame



            }); //end of currency command block
        }
        //User is typing in the wrong channel
        else
        {
            message.reply(`Please post command links in ${message.guild.channels.find("name", config.commandsChannel)}`);
        }


        ////////////////////////////////////////////////////////
        //              Start of Reaction Commands            //
        ////////////////////////////////////////////////////////

        connection.query("SELECT * FROM Members WHERE discordID = ?", [message.author.id], function (err, walletRows, fields)
        {
            if (err)
                return log(`err @ WalletChecking2: ${err}`);

            /* ??? 
             * Posts a reaction meme
             */
            if (command == "???")
            {
                //check for access
                if (!checkWalletAccess(walletRows[0].hasWalletAccess, message))
                    return;

                reactionMeme("questionMark.jpg", message);
            }


            /* spongecave
             * Posts a reaction meme
             */
            if (command == "spongecave")
            {
                //check for access
                if (!checkWalletAccess(walletRows[0].hasWalletAccess, message))
                    return;

                reactionMeme("caveSponge.jpg", message);
            }


            /* everyone
             * Posts a reaction meme
             */
            if (command == "everyone")
            {
                //check for access
                if (!checkWalletAccess(walletRows[0].hasWalletAccess, message))
                    return;

                reactionMeme("everyone.jpg", message);
            }


            /* gandalf 
             * Posts a reaction meme
             */
            if (command == "gandalf")
            {
                //check for access
                if (!checkWalletAccess(walletRows[0].hasWalletAccess, message))
                    return;

                reactionMeme("gandalf.gif", message);
            }


            /* persiancat
             * Posts a reaction meme
             */
            if (command == "persiancat")
            {
                //check for access
                if (!checkWalletAccess(walletRows[0].hasWalletAccess, message))
                    return;

                reactionMeme("PersianCat.jpg", message);
            }


            /* rage 
             * Posts a reaction meme
             */
            if (command == "rage")
            {
                //check for access
                if (!checkWalletAccess(walletRows[0].hasWalletAccess, message))
                    return;

                reactionMeme("RageMan.png", message);
            }

        });


        ////////////////////////////////////////////////////////
        //              Start of ADMIN Commands               //
        ////////////////////////////////////////////////////////

        //check for admin role
        if (!message.member.roles.has(adminRole.id)) { return };


        /* Kick
         * Kicks a member from the guild
         */
        if (command == "kick")
        {
            if (message.mentions.users.size == 0)
            {
                return message.reply("Please mention a user to kick");
            }
            let kickMember = message.guild.member(message.mentions.users.first());
            log(`${message.author} used KICK command in ${message.channel}, member: "${kickMember.user}"`);
            if (!kickMember)
            {
                return message.reply("User not valid!");
            }
            kickMember.kick().then(member =>
            {
                message.reply(`${member.user} got the boot!`);
            });
        }//end of kick


        /* Ban
         * Bans a member from the guild
         */
        if (command == "ban")
        {
            if (message.mentions.users.size == 0)
            {
                return message.reply("Please mention a user to ban");
            }
            let banMember = message.guild.member(message.mentions.users.first());
            let time = args[1];
            log(`${message.author} used BAN command in ${message.channel}, messageHistory: "${time}" days, member: "${banMember.user}"`);
            if (!banMember)
            {
                return message.reply("User not valid!");
            }
            if (time <= 7 || (time == 0 || time == null))
            {
                banMember.ban(time).then(member =>
                {
                    if (time == 0 || time == null)
                        message.reply(`${member.user.username}! recieved justice!`);
                    else
                        message.reply(`${member.user.username}! recieved justice. ${time} days of messages deleted!`)
                });
            }
            else
                message.reply("Please enter a value of 7 or less days!");
        }


        /* Give
         * Admin command for giving or taking away currency from members
         */
        if (command == "give")
        {
            if (message.mentions.users.size == 0)
                return;

            let amount = parseInt(args[0]);
            if (isNaN(amount))
                return;
            updateCurrency(message.mentions.users.first().id, amount);

            message.reply(`${message.mentions.users.first().username} has been given ${amount} Coins!`);
            log(`${message.author} gave ${message.mentions.users.first()} ${amount} Coins in ${message.channel}`);
        }//end of give


        /* Admin command for adding roles
         * Works for any roles present in the server
         */
        if (command == "adminaddrole" || command == "aar")
        {
            let roleArg = args[0];
            try
            {
                let roleMember = message.guild.member(message.mentions.users.first());
                let role = message.guild.roles.find("name", roleArg);
                roleMember.addRole(role.id);
                message.channel.send(`${roleMember.user} now has the role ${role.name}`);
                log(`${message.author} used addrole command in ${message.guild}, ${message.channel}, role: "${roleArg}", member: "${roleMember.user}"`);
            }
            catch (err)
            {
                message.reply("Invalid role or User!");
                log(`${message.author}  unsuccessfully used addrole command in ${message.guild}, #${message.channel}`);
                return;
            }
        } //end of aar


        /* deletegame
         * removes a gamerole from discord and sql database
         */
        if (command == "deletegame" | command == "dg")
        {
            let roleArg = args[0];
            try
            {
                //delete from discord
                let targetRole = message.guild.roles.find("name", roleArg);
                targetRole.delete();
                connection.query("DELETE FROM RoleList WHERE roleName = ?", [roleArg]);

                message.reply(`${roleArg} was successfully deleted from discord and the database!`);
                log(`${message.author} deleted role ${roleArg} from discord and RoleList Table`);
            }
            catch (err)
            {
                message.reply("Invalid role!");
                log(`${message.author} unsuccessfully used deletegame in ${message.channe}`);
            }
        }//end of deletegame


        /* editgame
         * edit a role in the database and on discord
         */
        if (command == "editgame" | command == "eg")
        {
            let roleArg = args[0];
            let newName = args[1];
            try
            {
                //delete from discord
                let targetRole = message.guild.roles.find("name", roleArg);
                targetRole.setName(newName);
                connection.query("UPDATE RoleList SET ? WHERE ?", [{ roleName: newName }, { roleName: roleArg }]);

                message.reply(`${roleArg} was changed to ${newName}`);
                log(`${message.author} changed ${roleArg} to ${newName}`);
            }
            catch (err)
            {
                message.reply("Invalid role!");
                log(`${message.author} unsuccessfully used editgame in ${message.channel}`);
            }
        }//end of deletegame


        /* Clear
         * clears a set number of previous messages in a channel
         */
        if (command == "clear")
        {
            numberOfLines = args[0];
            try
            {
                numberOfLines++;
                if (numberOfLines > 100) { message.reply("Please clear less than 100 lines"); return }

                message.channel.bulkDelete(numberOfLines, true);
                log(`${message.author} cleared ${numberOfLines - 1} lines in ${message.channel}`);
            }
            catch (err)
            {
                message.reply("Invalid input!");
                log(`${message.author} unsuccessfully used clear in ${message.channel}`);
            }
        }//end of clear


        /* getID
         * returns the ID of the selected user
         */
        if (command == "getid")
        {
            if (message.mentions.users.size == 0)
                return (message.reply("Please tag a user!"));

            message.channel.send(`${message.mentions.users.first()}'s ID is: ${message.mentions.users.first().id}`);
            log(`${message.author} used getID command in ${message.channel}`);
        }//end of getID


        /* nukeWallets
         * clears the wallet of every member
         */
        if (command == "nukewallets")
        {
            if (!(args[0] == "Yes" && args[1] == "I" && args[2] == "Am" && args[3] == "Sure"))
                return message.reply(`To active, after command type exactly "Yes I Am Sure"`);
            else
            {
                connection.query("SELECT * FROM Members", function (err, rows, fields)
                {
                    if (err)
                        return log(`err @ nukewallets: ${err}`);
                    for (var i = 0; i < rows.length; i++)
                    {
                        connection.query("UPDATE Members SET wealth = 0 WHERE discordID = ?", [rows[i].discordID]);
                    }
                });

            }
            message.channel.send("f to pay respect to everyone's coins.");
            log(`${message.author} wiped everyones money from channel ${message.channel}`);
        }//end of nukeWallets


        /* toggleWallet
         * toggles the wallet on or off for the
         * mentioned user
         */
        if (command == "togglewallet")
        {
            //checks for syntax
            if (message.mentions.users.size == 0)
                return (message.reply("Please mention a user!"));

            if (message.mentions.members.first().roles.has(adminRole.id) && !(message.author.id == 127492385696382976))
            {
                message.reply("You can't toggle an admin's wallet!");
            }

            //toggles wallet
            connection.query("SELECT * FROM Members WHERE discordID = ?", [message.mentions.users.first().id], function (err, rows, fields)
            {
                if (err)
                    return log(`err @ toggleWallet: ${err}`);
                var newValue = 3;
                var outputString = "NULL";
                //setup variables depending on current state
                if (rows[0].hasWalletAccess == 0)
                {
                    newValue = 1;
                    outputString = "on";
                }
                else
                {
                    newValue = 0;
                    outputString = "off";
                }

                //do the actually toggles and output
                connection.query("UPDATE Members SET hasWalletAccess = ? WHERE discordID = ?", [newValue, message.mentions.users.first().id]);
                message.channel.send(`${message.mentions.users.first()}'s has been toggled ${outputString}`);
                log(`${message.author} toggled ${message.mentions.users.first()}'s wallet ${outputString}`);
            });
        }


        /* Say
         * makes the bot say a message in a specific channel
         */
        if (command == "say")
        {
            //check for empty string in different channel
            if (args.join(" ") == "")
                return message.reply("Please enter a message");

            if (message.guild.channels.find("name", args[0]) != null)
            {
                let outputChannel = message.guild.channels.find("name", args[0]);
                args[0] = "";

                //check for empty string
                if (args.join(" ") == "")
                    return message.reply("Please enter a message");

                outputChannel.send(args.join(" "));
                message.reply(`Message sent to channel: #${outputChannel.name}`);
                log(`${message.author} used say command in ${message.channel}, message: "${args.join(" ")}" to channel: #${outputChannel}`);
            }
            else
            {
                message.channel.send(args.join(" "));
                log(`${message.author} used say command in ${message.channel}. Message: "${args.join(" ")}"`);
            }
        }


        /* UpdateBotCommands
         * wipes the message in bot commands and updates it to
         * the new text
         */
        if (command == "updatebotcommands")
        {
            let outputChannel = message.guild.channels.find("name", config.commandListChannel);
            outputChannel.bulkDelete(20);
            outputChannel.send(getHelpMessage());
            message.reply(`Updated text in ${outputChannel}`);
            log(`${message.author} used updatebotcommands in ${message.channel}`);
        }


        /* InviteUser
         * takes in 1 arg of a student ID then emails that student
         * an invite link
         */
        if (command == "inviteuser")
        {
            //check for input error
            if (isNaN(args[0]))
            {
                message.reply("Please enter a student ID");
                return log(`${message.author} incorrectly used InviteUser in ${message.channel}`)
            }

            //send the email
            emailUser(args[0], message);
        }


        /* InviteUser
         * invites all users from a list if they're not stored in
         * a table of previously invited users
         */
        if (command == "bulkinviteusers")
        {
            try
            {
                fs.createReadStream("assets/userInviteList.csv")
                    .pipe(csv())
                    .on("data", function (data)
                    {
                        //query database to see if user is already invited in bulk
                        connection.query("SELECT * FROM invitedUsers WHERE studentID = ?", [data], function (err, rows, fields)
                        {

                            if (err)
                            {
                                log("Error querying invitedUsers")
                            }

                            try
                            {
                                //trying to check for error
                                let tempID = rows[0].studentID;
                                log(`skipping student: ${data}`);
                            }
                            catch (err)
                            {
                                //add student ID to database and email
                                connection.query("INSERT INTO invitedUsers SET studentID = ?", [data], error =>
                                {
                                    if (error)
                                    {
                                        log(`Error adding student ID: ${error}`);
                                    }
                                    else
                                    {
                                        emailUser(data, null);
                                        log(`Invited student: ${data}`);
                                    }
                                });
                            }
                        });

                    })
                    .on("end", function (data)
                    {
                        message.reply(`found ${data} users in list, skipping previously invited ones.`);
                        log(`${message.author} used bulkInviteUsers in ${message.channel} from a list of ${data} users.`)
                    })
            }
            catch (err)
            {
                log("error reading userList file");
            }
        }
    }
});



//login with discord bot user token
bot.login(config.token);